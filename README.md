# notes-backend
## Deploy guide:
```git clone https://gitlab.com/major_ocelot/lewd-backend.git
cd notes-backend
mkdir instance
virtualenv -p python3.6 instance/venv
echo "export FLASK_APP=notes FLASK_ENV=development" >> instance/venv/bin/activate  # you can use "production" instead
. instance/venv/bin/activate
pip install -r "requirements/${FLASK_ENV}_requirements.txt"
flask run -p 1337
```
