import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from notes.helpers import jsonify_errors

app = Flask(__name__)

app.config.from_pyfile('{}_config.py'.format(os.path.join(app.root_path, 'configs', os.environ.get('FLASK_ENV'))))
app.config.from_pyfile(os.path.join(app.instance_path, 'instance_config.py'), silent=True)

db = SQLAlchemy(app)

import notes.models

db.create_all()

from notes.api.v1 import api_v1

app.register_blueprint(api_v1)


@app.errorhandler(404)
def not_found_handler(exception):
    return jsonify_errors('Not found.'), 404


@app.errorhandler(405)
def method_not_allowed_handler(exception):
    return jsonify_errors('Method not allowed.'), 405
