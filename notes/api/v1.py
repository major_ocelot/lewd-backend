from flask import Blueprint, request, jsonify
from flask_httpauth import HTTPBasicAuth

from notes import db
from notes.helpers import jsonify_errors, jsonify_data
from notes.models import User, Note

api_v1 = Blueprint('api_v1', __name__, url_prefix='/api/v1')
auth = HTTPBasicAuth()


@auth.verify_password
def verify_password(username, password):
    user = User.query.filter_by(username=username).first()

    if not user:
        return False

    return user.verify_password(password)


@api_v1.route('/users', methods=['POST'])
def register():
    json = request.get_json()
    if not json:
        return jsonify_errors('No JSON in request.'), 400

    data = json.get('data')
    if not data:
        return jsonify_errors('No `data` member at request\'s top level.'), 422

    username = data.get('username')
    password = data.get('password')
    shown_name = data.get('shown_name')

    if not username or not password:
        return jsonify_errors('Both username and password required to register.'), 400

    if User.query.filter_by(username=username).first():
        return jsonify_errors('User "{}" exists.'.format(username)), 403

    user = User(username=username, password=password, shown_name=shown_name)
    db.session.add(user)
    db.session.commit()

    return jsonify_data({'id': user.id})


@api_v1.route('/users/<int:user_id>', methods=['GET'])
@auth.login_required
def get_user(user_id):
    if user_id == 0:
        user = User.query.filter_by(username=auth.username()).first()
    else:
        user = User.query.get(user_id)

    if not user:
        return jsonify_errors('User {} not found.'.format(user_id)), 404

    if auth.username() != user.username and not user.is_admin:
        return jsonify_errors('You are not authorized to get this user\'s information.'), 401

    return jsonify_data({
        'id': user.id,
        'username': user.username,
        'shown_name': user.shown_name,
    })


@api_v1.route('/users/<int:user_id>', methods=['PATCH'])
@auth.login_required
def patch_user(user_id):
    if user_id == 0:
        user = User.query.filter_by(username=auth.username()).first()
    else:
        user = User.query.get(user_id)

    if not user:
        return jsonify_errors('User {} not found.'.format(user_id)), 404

    if auth.username() != user.username and not user.is_admin:
        return jsonify_errors('You are not authorized to get this user\'s information.'), 401

    json = request.get_json()
    if not json:
        return jsonify_errors('No JSON in request.'), 400

    data = json.get('data')
    if not data:
        return jsonify_errors('No `data` member at request\'s top level.'), 422

    password = data.get('password')
    shown_name = data.get('shown_name')

    if password:
        user.password = password
    if shown_name:
        user.shown_name = shown_name

    db.session.commit()

    return jsonify_data({'id': user.id})


@api_v1.route('/users/<int:user_id>', methods=['DELETE'])
@auth.login_required
def delete_user(user_id):
    if user_id == 0:
        user = User.query.filter_by(username=auth.username()).first()
    else:
        user = User.query.get(user_id)

    if not user:
        return jsonify_errors('User {} not found.'.format(user_id)), 404

    if auth.username() != user.username and not user.is_admin:
        return jsonify_errors('You are not authorized to get this user\'s information.'), 401

    Note.query.filter_by(owner=user.id).delete()
    db.session.delete(user)
    db.session.commit()

    return jsonify({'id': None})


@api_v1.route('/users/<int:user_id>/notes', methods=['GET'])
@auth.login_required
def get_notes(user_id):
    if user_id == 0:
        user = User.query.filter_by(username=auth.username()).first()
    else:
        user = User.query.get(user_id)

    if not user:
        return jsonify_errors('User {} not found.'.format(user_id)), 404

    if auth.username() != user.username and not user.is_admin:
        return jsonify_errors('You are not authorized to get this user\'s information.'), 401

    notes = Note.query.filter_by(owner=user.id).all()
    notes_list = []
    for note in notes:
        notes_list.append({
            'id': note.id,
            'title': note.title,
            'tags': note.tags.split(),
            'created': note.created,
            'modified': note.modified,
        })
    return jsonify_data(*notes_list)


@api_v1.route('/users/<int:user_id>/notes', methods=['POST'])
@auth.login_required
def new_note(user_id):
    if user_id == 0:
        user = User.query.filter_by(username=auth.username()).first()
    else:
        user = User.query.get(user_id)

    if not user:
        return jsonify_errors('User {} not found.'.format(user_id)), 404

    if auth.username() != user.username and not user.is_admin:
        return jsonify_errors('You are not authorized to get this user\'s information.'), 401

    json = request.get_json()
    if not json:
        return jsonify_errors('No JSON in request.'), 400

    data = json.get('data')
    if not data:
        return jsonify_errors('No `data` member at request\'s top level.'), 422

    title = data.get('title')
    text = data.get('text')
    tags = data.get('tags')

    if not title:
        return jsonify_errors('A title is required to create a note.'), 401

    for tag in tags:
        if ' ' in tag:
            return jsonify_errors('Tags could not contain spaces.'), 401

    note = Note(owner=user.id, title=title, text=text, tags=' '.join(tags) if tags else None)

    db.session.add(note)
    db.session.commit()

    return jsonify_data({'id': note.id})


@api_v1.route('/users/<int:user_id>/notes/<int:note_id>', methods=['GET'])
@auth.login_required
def get_note(user_id, note_id):
    if user_id == 0:
        user = User.query.filter_by(username=auth.username()).first()
    else:
        user = User.query.get(user_id)

    if not user:
        return jsonify_errors('User {} not found.'.format(user_id)), 404

    if auth.username() != user.username and not user.is_admin:
        return jsonify_errors('You are not authorized to get this user\'s information.'), 401

    note = Note.query.get(note_id)
    if not note:
        return jsonify_errors('Note {} not found.'.format(note_id)), 404

    return jsonify_data({
        'title': note.title,
        'text': note.text,
        'tags': note.tags.split(' '),
        'created': note.created,
        'modified': note.modified,
    })


@api_v1.route('/users/<int:user_id>/notes/<int:note_id>', methods=['PATCH'])
@auth.login_required
def patch_notes(user_id, note_id):
    if user_id == 0:
        user = User.query.filter_by(username=auth.username()).first()
    else:
        user = User.query.get(user_id)

    if not user:
        return jsonify_errors('User {} not found.'.format(user_id)), 404

    if auth.username() != user.username and not user.is_admin:
        return jsonify_errors('You are not authorized to get this user\'s information.'), 401

    json = request.get_json()
    if not json:
        return jsonify_errors('No JSON in request.'), 400

    data = json.get('data')
    if not data:
        return jsonify_errors('No `data` member at request\'s top level.'), 422

    title = data.get('title')
    text = data.get('text')
    tags = data.get('tags')

    note = Note.query.get(note_id)
    if not note:
        return jsonify_errors('Note {} not found.'.format(note_id)), 404

    if title:
        note.title = title
    if text:
        note.text = text
    for tag in tags:
        if ' ' in tag:
            return jsonify_errors('Tags could not contain spaces.'), 401
    note.tags = ' '.join(tags)

    db.session.commit()

    return jsonify_data({'id': note.id})


@api_v1.route('/users/<int:user_id>/notes/<int:note_id>', methods=['DELETE'])
@auth.login_required
def delete_note(user_id, note_id):
    if user_id == 0:
        user = User.query.filter_by(username=auth.username()).first()
    else:
        user = User.query.get(user_id)

    if not user:
        return jsonify_errors('User {} not found.'.format(user_id)), 404

    if auth.username() != user.username and not user.is_admin:
        return jsonify_errors('You are not authorized to get this user\'s information.'), 401

    note = Note.query.get(note_id)
    if not note:
        return jsonify_errors('Note {} not found.'.format(note_id)), 404

    db.session.delete(note)
    db.session.commit()

    return jsonify({'id': None})
