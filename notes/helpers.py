from flask import jsonify


def jsonify_data(*args):
    return jsonify({'data': [data for data in args]})


def jsonify_errors(*args):
    return jsonify({'errors': [{'detail': detail} for detail in args]})
