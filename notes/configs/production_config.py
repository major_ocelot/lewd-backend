import os

from notes import app
from notes.configs.common_config import *

FLASK_DEBUG = False
SQLALCHEMY_DATABASE_URI = 'sqlite:///{}'.format(os.path.join(app.instance_path, 'production.sqlite'))
