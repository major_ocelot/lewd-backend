import os

from notes import app
from notes.configs.common_config import *

FLASK_DEBUG = True
SQLALCHEMY_DATABASE_URI = 'sqlite:///{}'.format(os.path.join(app.instance_path, 'development.sqlite'))
